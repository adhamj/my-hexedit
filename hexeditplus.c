#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
/*============================================================
 *============(Declerations)========================================*/

void set_file_name();
void set_unit_size();
void quit_hex();
void file_display();
void file_modify();
void file_copy();
void (*functions[6])() = {set_file_name , set_unit_size  ,file_display,file_modify, file_copy, quit_hex};

char* one_size;
unsigned short* two_size;
unsigned int* four_size;
char file_name[101];
int size;
int i;
/*=========================================================
 *============(Main Loop)=====================================*/

int main(int argc, char *argv[]) {
    while(1){
        printf("Choose action:\n1-Set File Name\n2-Set Unit Size\n3-File Display\n4-File Modify\n5-Copy From File\n6-Quit\n");
        scanf("%d",&i);
        (*functions[i-1])();
    }
}


/*========================================================
 ==========(Implementation)====================================*/



void file_modify(){
    int location,fd;
    char* value = (char*)malloc(1);
     if ( (fd =open(file_name,O_RDWR)) < 0  ){
        printf("there was a problem opening said file\n");
        return;
    }
    printf("Please enter <location> <val>\n");
    scanf("%x %x",&location,value);
    lseek(fd,location,SEEK_SET);
    write(fd,value,size);
    free(value);
    close(fd);
    
}


/* Set File Name queries the user for a file name,
 * and store it in a globally accessible buffer.*/
void set_file_name(){
    while(getchar()!= '\n');
    fgets(file_name,101,stdin);
    strtok(file_name,"\n");
}
/*The Set Unit Size option sets the size variable.*/
void set_unit_size(){
    int i;
    scanf("%d",&i);
    switch(i){
        case 1:
            size = 1;
            break;
        
        case 2:
            size = 2;
            break;
        
        case 4:
            size = 4;
            break;
            
        default:
            printf("illegal size");
            break;
    }
}

/*quit the program.*/
void quit_hex(){
    exit(0);
}


/*This option displays length units from the file filename
 * (chosen using option 1 in the menu), 
 * starting at file location location 
 * (note: this is the same as the "offset" in the file).
 * The units should be displayed once using a hexadecimal representation,
 * and again using a decimal representation.*/
void file_display(){

    int length, location;
    char line[2048];
    if(!*file_name){
        printf("error: file name is null");
        return;
    }
    int fd;
    if ( (fd =open(file_name,O_RDWR)) < 0  ){
        printf("there was a problem opening said file\n");
        return;
    }
    
    printf("Please enter <location> <length>\n");
    while(getchar()!= '\n');
    fgets(line,2048,stdin);
    sscanf(line,"%x %d",&location,&length);
    
    lseek(fd,location,SEEK_SET);
    char* read_data = (char*)malloc(size*length);
    read(fd,read_data,length*size);
    close(fd);
    
    printf("Hexadecimal Representation:\n");
    for(int i = 0 ;  i < length ; i++){
        switch(size){
            case 1:
            {
                one_size = (char*)read_data;
                printf("%02x ",one_size[i]);
                break;
            }
            case 2:
                two_size = (unsigned short*)read_data;
                printf("%04x ",two_size[i]);
                break;
            case 4:
                four_size = (unsigned int*)read_data;
                printf("%08x ",four_size[i]);
                break;
        }
    }
    printf("\nDecimal Representation:\n");
    for(int i = 0 ;  i < length ; i++){
        switch(size){
            case 1:
            {
                one_size = (char*)read_data;
                printf("%ld ",(long int)one_size[i]);
                break;
            }
            case 2:
                two_size = (unsigned short*)read_data;
                printf("%ld ",(long int)two_size[i]);
                break;
            case 4:
                four_size = (unsigned int*)read_data;
                printf("%ld ",(long int)four_size[i]);
                break;
        }
    }
    printf("\n");
    free(read_data);
}

void file_copy(){
    char line[2048];;
    char source[100];
    int source_offset,dst_offset,length;
    printf("Please enter <src_file> <src_offset> <dst_offset> <length>\n");
    while(getchar()!= '\n');
    printf("\n");
    fgets(line,2048,stdin);
    sscanf(line,"%s %x %x %d",source,&source_offset,&dst_offset,&length);
    strtok(source,"\n");
    int fd;
    if ( (fd =open(source,O_RDWR)) < 0  ){
        printf("there was a problem opening said file\n");
        return;
    }
    lseek(fd,source_offset,SEEK_SET);
    char* read_data = (char*)malloc(size*length);
    read(fd,read_data,length*size);
    close(fd);
    if ( (fd =open(file_name,O_RDWR)) < 0  ){
        printf("there was a problem opening said file\n");
        return;
    }
    lseek(fd,dst_offset,SEEK_SET);
    write(fd,read_data,length*size);
    free(read_data);
}